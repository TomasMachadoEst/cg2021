// BasicOGL.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include "Triangulo.h" 
#include "Quadrado.h" 
#include "Sistema.h" // Como est� dentro do projecto j� o includo com " " --> toda a biblioteca que inclui a glut tem que ser a ultima  ser implementada

// O objecto tem que ser global para desenhar na callback desenho 

//Triangulo* oTriangulo1 = new Triangulo(-17,8,-14,18,-11,8); // Criacao de objectos, o * quer dizer apontador para memoria //
Triangulo* oTriangulo2 = new Triangulo(-6,6,0,12,6,6);
//Triangulo* oTriangulo3 = new Triangulo(10,5,10,7,16,5);
Quadrado* oQuadrado1 = new Quadrado(-6,-6,-6,6,6,6,6,-6,0.0,0.0,1.0);
Quadrado* oQuadrado2 = new Quadrado(2,2, 2, 4, 4, 4, 4, 2,0.1,0.0,0.0);
Quadrado* oQuadrado3 = new Quadrado(6, 6, 4, 8, 4, 12, 6, 12,0.0,0.0,1.0);



// Callback Teclado
void teclado(unsigned char tecla, int x , int y) {

    if (tecla == 27)
        exit(0);


}

//Callback desenho
void desenho(void) {

    glClearColor(0, 0, 0, 0);  // definir a cor de fundo --> preto
    glClear(GL_COLOR_BUFFER_BIT);  // limpar o buffer de cor

    glMatrixMode(GL_PROJECTION); // sela��o da matriz de projecao
    glLoadIdentity(); // carregar a matriz identidade 

    gluOrtho2D(-20.0f,20.0f,-20.0f,20.0f); // definicao da tela = mundo de desenho de -20 a 20

    glMatrixMode(GL_MODELVIEW); // Matriz de visualiza��o
    glLoadIdentity(); 

    //oTriangulo1->desenho(); // Invoca o metodo desenho no nosso objecto
    oTriangulo2->desenho();
    //oTriangulo3->desenho();
    oQuadrado1->desenho();
    oQuadrado2->desenho();
    oQuadrado3->desenho();


    glutSwapBuffers(); // So fa�o a troca de buffers quando acabo de desenhar tudo. 

}




int main(int argc , char ** argv)
{
    // Bloco de configura��o Inicial da freeGLUT e da janela de vizualiza��o

    glutInit(&argc, argv); // Inicializa��o do freeGLUT

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE); // Inicializa��o do modo de renderiza��o #### GLUT_DOUBLE -> Modo de duplo buffer, tenho que trocar os buffers para aparecer o desenho

    glutInitWindowSize(1000, 1000); // Definir o tamanho da janela

    glutInitWindowSize(0, 0); // Posi��o inicial da janela

    glutCreateWindow("Tri�ngulo"); // Criar a janela

    

    // Registo das Callback

    // callback desenho


    glutDisplayFunc(desenho);


    // callback teclado

    glutKeyboardFunc(teclado);

    

    glutMainLoop(); // Iniciar o ciclo de eventos, so reage aos eventos programados

    
    return 0;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
