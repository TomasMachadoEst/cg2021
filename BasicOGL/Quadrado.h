#pragma once

#include "Sistema.h"


class Quadrado
{
public:

	float quadVertex[4][2];

	float quadColor[3]; // Vai me armazenar de componenetes, ou seja 4 cores


	Quadrado(float, float, float, float, float, float,float,float,float, float, float); // aqui so meto a assinatura, so meto o tipo
	
	void desenho(void);

	~Quadrado(); // Destrutor
};

