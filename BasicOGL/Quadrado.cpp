#include "Quadrado.h"


Quadrado::Quadrado(float v1x, float v1y, float v2x, float v2y, float v3x, float v3y, float v4x, float v4y,float c1, float c2, float c3){

	//Vertice 1 
	quadVertex[0][0] = v1x;
	quadVertex[0][1] = v1y;

	//Vertice 2
	quadVertex[1][0] = v2x;
	quadVertex[1][1] = v2y;

	//Vertice 3
	quadVertex[2][0] = v3x;
	quadVertex[2][1] = v3y;

	//Vertice 4
	quadVertex[3][0] = v4x;
	quadVertex[3][1] = v4y;

	// o construtor tem que deixar sempre o objecto completo , por isso temos que meter tamb�m a cor

	// D� cor ao quadrado
	quadColor[0] = c1;
	quadColor[1] = c2;
	quadColor[2] = c3;

}

void Quadrado::desenho(void) {

	glColor3fv(quadColor); // se nao tiver esta linha por definicao desenha a branco

	glBegin(GL_QUADS); {

		//glVertex , 2 � o numero de coordenadas,f o tipo de dados, v vector

		glVertex2fv(quadVertex[0]);
		glVertex2fv(quadVertex[1]);
		glVertex2fv(quadVertex[2]);
		glVertex2fv(quadVertex[3]);

	}glEnd();
}


Quadrado::~Quadrado() {


}