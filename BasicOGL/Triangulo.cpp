// Aqui vai incluir todos os metodos da class


#include "Triangulo.h" 


// O metodo faz parte da class com o nome da class e :: � frente


Triangulo::Triangulo(){

	// D� cor ao triangulo
	triangleColor[0] = 1.0;
	triangleColor[1] = 0.0;
	triangleColor[2] = 0.0;

	//� sempre desenhado no sentido dos ponteiros do rel�gio

	//Vertice 1 
	triangleVertex[0][0] = 4.0;
	triangleVertex[0][1] = 0.0;

	//Vertice 2
	triangleVertex[1][0] = 0.0;
	triangleVertex[1][1] = 0.0;

	//Vertice 3
	triangleVertex[2][0] = 2.0;
	triangleVertex[2][1] = 4.0;

	
}
// aqui no cpp meto o tipo e o nome do parametro para o poder manipular

Triangulo::Triangulo(float  v1x, float v1y, float v2x, float v2y, float v3x, float v3y) {

	//Vertice 1 
	triangleVertex[0][0] = v1x;
	triangleVertex[0][1] = v1y;

	//Vertice 2
	triangleVertex[1][0] = v2x;
	triangleVertex[1][1] = v2y;

	//Vertice 3
	triangleVertex[2][0] = v3x;
	triangleVertex[2][1] = v3y;

	// o construtor tem que deixar sempre o objecto completo , por isso temos que meter tamb�m a cor

	// D� cor ao triangulo
	triangleColor[0] = 1.0;
	triangleColor[1] = 0.0;
	triangleColor[2] = 0.0;

}

// metodo responsavel por desenhar o triangulo, e s� desenha o triangulo quando � invocado.

void Triangulo::desenho(void) {

	glColor3fv(triangleColor); // se nao tiver esta linha por definicao desenha a branco

	glBegin(GL_TRIANGLES); {

		//glVertex , 2 � o numero de coordenadas,f o tipo de dados, v vector
		
		glVertex2fv(triangleVertex[0]);
		glVertex2fv(triangleVertex[1]);
		glVertex2fv(triangleVertex[2]);

	}glEnd();


}


Triangulo::~Triangulo(){



}