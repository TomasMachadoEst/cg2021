// Ficha2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Sistema.h"
#include "Nave.h"

Nave* nave = new Nave(10.0f,5.0f);

void teclado(unsigned char tecla, int x, int y) {

    switch (tecla) {

    case 27:
        exit(0);

    case "a":
        nave->mover(0);

    case "w":

        nave->mover(1);

    case "d":
        nave->mover(2);

    case "s":
        nave->mover(3);

    }
}

//Callback desenho
void desenho(void) {

    glClearColor(0, 0, 0, 0);  // definir a cor de fundo --> preto
    glClear(GL_COLOR_BUFFER_BIT);  // limpar o buffer de cor

    glMatrixMode(GL_PROJECTION); // selação da matriz de projecao
    glLoadIdentity(); // carregar a matriz identidade 

    gluOrtho2D(-20.0f, 20.0f, -20.0f, 20.0f); // definicao da tela = mundo de desenho de -20 a 20

    glMatrixMode(GL_MODELVIEW); // Matriz de visualização
    glLoadIdentity();

    

    nave->desenho();
    
 

    glutSwapBuffers(); // So faço a troca de buffers quando acabo de desenhar tudo. 

}


int main(int argc, char** argv)
{
    glutInit(&argc, argv); // Inicialização do freeGLUT

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE); // Inicialização do modo de renderização #### GLUT_DOUBLE -> Modo de duplo buffer, tenho que trocar os buffers para aparecer o desenho

    glutInitWindowSize(1000, 1000); // Definir o tamanho da janela

    glutInitWindowSize(0, 0); // Posição inicial da janela

    glutCreateWindow("Transformacoes Geometricas"); // Criar a janela



    // Registo das Callback

    // callback desenho


   glutDisplayFunc(desenho);

  

    // callback teclado

    glutKeyboardFunc(teclado);



    glutMainLoop(); // Iniciar o ciclo de eventos, so reage aos eventos programados


    return 0;

}
