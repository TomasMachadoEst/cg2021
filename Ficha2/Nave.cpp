#include "Nave.h"
#include "Sistema.h"


Nave::Nave(){

	position[0] = position[1] = 0.0;

}

Nave::Nave(GLfloat px, GLfloat py) {

	position[0] = px;
	position[1] = py;

}


void Nave::mover(GLint m) {

	GLfloat speedx = 0.5;
	GLfloat speedy = 0.5;

	switch(m)
	{
	case'0':
		position[0] = position[0] - speedx;
	case'1':
		position[1] = position[1] + speedy;
	case'2':
		position[0] = position[0] + speedx;
	case'3':
		position[1] = position[1] - speedy;
	}
}


void Nave::cockpit(){

	glBegin(GL_TRIANGLES); {

		glVertex2f(0.0f, 0.0f);
		glVertex2f(1.0f, 2.0f);
		glVertex2f(2.0f, 0.0f);
	}glEnd();


}
void Nave::asaEsquerda(){

	glBegin(GL_TRIANGLES); {
		glVertex2f(0.0f, 0.0f);
		glVertex2f(2.0f, 1.0f);
		glVertex2f(2.0f, 0.0f);

	}glEnd();
}
void Nave::corpoNave(){

	glBegin(GL_QUADS); {

		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 2.0f);
		glVertex2f(2.0f, 2.0f);
		glVertex2f(2.0f, 0.0f);
	}glEnd();


}
void Nave::asaDireita() {

	glBegin(GL_TRIANGLES); {

		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 1.0f);
		glVertex2f(2.0f, 0.0f);

	}glEnd();
}
// como � um medo de ajuda n�o deve estar incluido na classe Nave.h
void ajudaEixos() {
	//desenhar os eixos
	glColor3f(1.0f, 1.0f, 1.0f);

	glBegin(GL_LINES); {
	
		glVertex2f(0.0f, 0.0f);
		glVertex2f(2.0f, 0.0f);

		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 2.0f);
	
	}

	glEnd();
}

void Nave::desenho() {
	// em vez de ter glMatrixIden temos translacoes controladas com pilha


	// ajudaEixos();

	glColor3f(1.0f, 0.0f, 0.0f);
	
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	// onde eu quero colocar a nave, que vai servir para fazer o resto das translacoes,
	// ou seja � uma translacao que soma com todas as outras translacoes


	glTranslatef(position[0],position[1], 0.0f);
	glPushMatrix();

	glTranslatef(-1.0f, -2.0f, 0.0f);
	corpoNave();
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-3.0f, -1.0f, 0.0f);
	asaEsquerda();
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(1.0f, -1.0f, 0.0f);
	asaDireita();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.0f, 0.0f, 0.0f);
	cockpit();
	glPopMatrix();
	glPopMatrix();

}
Nave::~Nave(){

}
