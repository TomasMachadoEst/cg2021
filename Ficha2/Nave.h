#pragma once
#include "Sistema.h"
class Nave
{

private:
	// privados para que n�o posso alterar ao invocar a classe desenho
	
	GLfloat position[2];

	GLvoid cockpit();
	GLvoid asaDireita();
	GLvoid asaEsquerda();
	GLvoid corpoNave();

public:
	Nave();

	void mover(GLint);
	Nave(GLfloat, GLfloat);
	GLvoid desenho(GLvoid);
	~Nave();

};

