#pragma once
#include "Sistema.h"

class Bullet
{

private:
	
	GLfloat position[2]; //posicao inicial 
	GLfloat speedy1, speedy2;

	GLvoid Bullet_shape();

public:

	Bullet();
	Bullet(GLfloat, GLfloat);
	GLfloat* getPosition(GLvoid);
	GLboolean move(GLint);
	GLvoid draw(GLvoid);
	~Bullet();


};


