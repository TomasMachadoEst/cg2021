#include "Bullet.h"


extern GLfloat worldcoor[4];


Bullet::Bullet() {
	
  position[0] = position[1] = 0.0f;

  GLfloat speedy1 = 1.0f;
  GLfloat speedy2 = 0.5f;

}


Bullet::Bullet(GLfloat px, GLfloat py) {

	position[0] = px;
	position[1] = py;
	speedy1 = 1.0f;
	speedy2 = 0.5f;
}


GLfloat* Bullet::getPosition(GLvoid) {

	return position;
}




GLboolean Bullet::move(GLint dir) {
	switch (dir)
	{
	case 0: // bullet ship
		if ((position[1] + speedy1 + 0.3f) >= worldcoor[2])
		{
		position[1] += speedy1;
		return true;

		}break;

	case 1: //  bullet warrior
		if ((position[1] - speedy2 - 0.3f) <= worldcoor[3])
		{
			position[1] -= speedy2;
			return true;

		}break;
		
	default:
		return false;
		break;

	}return false;
}



GLvoid Bullet::Bullet_shape(GLvoid) {
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS); {
		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 0.6f);
		glVertex2f(0.1f, 0.6f);
		glVertex2f(0.1f, 0.0f);
	}
	glEnd();
}


GLvoid Bullet::draw(GLvoid) {

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glTranslatef(position[0], position[1], 0);
	glPushMatrix();

	glTranslatef(-0.05f, -0.3f, 0.0f);
	Bullet_shape();

	glPopMatrix();
	glPopMatrix();

}





Bullet::~Bullet(){

}