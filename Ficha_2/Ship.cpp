#include "Ship.h"
#include "Sistema.h"

extern GLfloat worldcoor[4];


Ship::Ship() {

	position[0] = position[1] = 0.0;
	GLfloat speedx = 0.8f;
	GLfloat speedy = 0.8f;
}

Ship::Ship(GLfloat px, GLfloat py) {

	position[0] = px;
	position[1] = py;
	speedx = 0.8f;
	speedy = 0.8f;

}

GLfloat* Ship::getPosition(GLvoid) {

	return position;
}

GLboolean Ship::move(GLint dir) {



	switch (dir)
	{
		//w
	case 0:
		if ((position[1] + speedy + 1.5f ) < worldcoor[3])
		{
			position[1] += speedy;
			return true;
			
		}break;
		//a
	case 1:
		if ((position[0] - speedx  - 2.5f ) > worldcoor[0])
		{
			position[0] -= speedx;
			return true;
			
		}break;
		//s
	case 2:
		if ((position[1] - speedy - 1.5f) > worldcoor[2])
		{
			position[1] -= speedy;
			return true;
			
		}break;
		//d
	case 3:
		if ((position[0] + speedx + 2.5f) < worldcoor[1])
		{
			position[0] += speedx;
			return true;
			
		}break;
	default:
		return false;
		break;

	}
	return false;
}


void Ship::cockpit() {


	glBegin(GL_TRIANGLES); {

		glColor3f(0.1f, 0.1f, 0.1f);


		glVertex2f(0.0f, 0.0f);
		glVertex2f(1.0f, 2.0f);
		glVertex2f(2.0f, 0.0f);
	}glEnd();


}
void Ship::leftWing() {

	glColor3f(0.4f, 0.4f, 0.2f);

	glBegin(GL_TRIANGLES); {
		glVertex2f(0.0f, 0.0f);
		glVertex2f(2.0f, 1.0f);
		glVertex2f(2.0f, 0.0f);

	}glEnd();
}
void Ship::shipBody() {

	glColor3f(1.0f, 1.0f, 0.5f);

	glBegin(GL_QUADS); {

		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 2.0f);
		glVertex2f(2.0f, 2.0f);
		glVertex2f(2.0f, 0.0f);
	}glEnd();


}
void Ship::rightWing() {

	glColor3f(0.4f, 0.4f, 0.2f);

	glBegin(GL_TRIANGLES); {

		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 1.0f);
		glVertex2f(2.0f, 0.0f);

	}glEnd();
}
// como � um metodo de ajuda n�o deve estar incluido na classe Nave.h
void axisHelper() {
	//desenhar os eixos
	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin(GL_LINES); {

		glVertex2f(0.0f, 0.0f);
		glVertex2f(6.0f, 0.0f);

		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 4.0f);

	}

	glEnd();
}

GLvoid Ship::draw() {
	// em vez de ter glMatrixIden temos translacoes controladas com pilha


	 //axisHelper();




	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	// onde eu quero colocar a nave, que vai servir para fazer o resto das translacoes,
	// ou seja � uma translacao que soma com todas as outras translacoes


	glTranslatef(position[0], position[1], 0.0f);
	glPushMatrix();

	glTranslatef(-1.0f, -2.0f, 0.0f);
	shipBody();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.0f, -1.0f, 0.0f);
	leftWing();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1.0f, -1.0f, 0.0f);
	rightWing();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.0f, 0.0f, 0.0f);
	cockpit();
	glPopMatrix();
	glPopMatrix();

}
Ship::~Ship() {


}
