#pragma once
#include "Sistema.h"
class Ship
{

private:
	// privados para que n�o posso alterar ao invocar a classe desenho

	GLfloat position[2];
	GLfloat speedx, speedy;

	GLvoid cockpit();
	GLvoid rightWing();
	GLvoid leftWing();
	GLvoid shipBody();

public:
	Ship();

	GLboolean move(GLint);
	Ship(GLfloat, GLfloat);
	GLfloat * getPosition(GLvoid);
	GLvoid draw(GLvoid);
	~Ship();

};
