// Ficha2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Sistema.h"
#include "Warrior.h"
#include "Ship.h"
#include "Bullet.h";


using namespace std;

extern GLfloat worldcoor[4] = { -30.0f, 30.0f, -30.0f, 30.0f }; 

Ship* ship = new Ship(0.0f, -25.0f); // posicao onde vou colocar inicialmente a ship

GLint dirWarrior = 0; // lado para o qual os warriors se vao mover
GLint warriorsidecolision = 0; // variavel que me vai fazer mover os warriors para baixo depois de 5 vezes a bater de lado

GLint life = 5;
GLint score = 0;
GLboolean pause = false;
GLboolean lose = false;

stringstream Str1;
char char1;

GLboolean gameTimerExpired = false;

// assim o glutPostRedisplay � mais facil de chamar
GLboolean needsRedisplay = false;
vector <Warrior*> warriors;
vector <Bullet*> bullets;
vector <Bullet*> bulletsWarriors;


GLint counter1 = 0;
// counter para contar o numere de vezes que a idle � chamada
GLint counter2 = 0;

//funcao para criar os warriors
GLvoid creatWarriors(GLvoid) {
    GLfloat px, py = 0.0f;
   
    for (GLint i = 0; i < 33; i++) {
    
        px = -20 + (i % 11) * 2.0f;
        py = 25 +(i % 3) * 2.0f;
        warriors.push_back(new Warrior (px, py));
    }


}


GLvoid keyboard(unsigned char key, int x, int y) {

    GLboolean hasMoved = false;

    switch (key) {

    case 27:
        exit(0);
        break;

    case 'a':
    case 'A':

        if (pause)
            break;
         hasMoved = ship->move(1);

        break;

    case 'w':
    case 'W':

        if (pause)
            break;
        hasMoved = ship->move(0);
        break;

    case 'd':
    case 'D':

        if (pause)
            break;
        hasMoved = ship->move(3);
        break;

    case 's':
    case 'S':

        if (pause)
            break;
        hasMoved = ship->move(2);
        break;

    case 'e':
    case 'E':

        if (pause)
            break;
            bullets.push_back(new Bullet(ship->getPosition()[0], ship->getPosition()[1]));
        break;
  
    case 'p':
    case 'P':

        if (!pause)
            pause = true;
        else
            pause = false;
        break;

    default:
        break;
    }
    if (hasMoved)
        needsRedisplay = true;
}

GLvoid drawLose(GLvoid) {
    glColor3f(0.0, 1.0, 0.0);
    glRasterPos2f(-10.0, 0.0);

    Str1.clear();
    //Str1 << "YOU LOSE, 'ESC' to leave.  Score: " << score << endl; <-- a correta 
    Str1 << "YOU LOSE NOOB!!!, 'ESC' to uninstall.  Score: " << score << endl; // <-- joke


    while (Str1.get(char1)) {
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, char1);
    }
}

GLvoid drawScore(GLvoid) {

    glColor3f(1.0, 1.0, 1.0);
    glRasterPos2f(-30.0, 28.0);

    Str1.clear();
    Str1 << "Score: " << score << endl;

    while (Str1.get(char1)) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, char1);
    }


}

GLvoid drawLife(GLvoid) {

    glColor3f(0.0, 1.0, 0.0);
    glRasterPos2f(-30.0, 29.0);

    Str1.clear();
    Str1 << "Life : " << life << endl;

    while (Str1.get(char1)) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, char1);
    }
}

GLvoid addScore(GLvoid) {
    score += 5;
    counter1 += 5;
    cout << life << endl;
    if (counter1 == 100 && life < 5 && life > 0) { 
        life++;
        counter1 = 0;
    }

}


GLvoid gameTimer(GLint value) {

    gameTimerExpired = true;
    glutTimerFunc(17, gameTimer, 0);


}

GLvoid shipWarriorColision() {

    GLfloat shipXMax = ship->getPosition()[0] + 3.0f;
    GLfloat shipXMin = ship->getPosition()[0] - 3.0f;
    GLfloat shipYMax = ship->getPosition()[1] + 2.0f;
    GLfloat shipYMin = ship->getPosition()[1] - 2.0f;

   



    GLfloat warriorXMax = 0;
    GLfloat warriorXMin = 0;
    GLfloat warriorYMax = 0;
    GLfloat warriorYMin = 0;

    for (int i = 0; i < warriors.size(); i++) {

        warriorXMax = warriors[i]->getPosition()[0] + 0.5f;
        warriorXMin = warriors[i]->getPosition()[0] - 0.5f;
        warriorYMax = warriors[i]->getPosition()[1] + 0.8f;
        warriorYMin = warriors[i]->getPosition()[1] - 0.8f;

        if (!(warriorXMax <= shipXMin || warriorYMin >= shipYMax || warriorXMin >= shipXMax || warriorYMax <= shipYMin)) {

            warriors.erase(warriors.begin() + i);
            life--;
            i = 0;
            if (life < 1) {
                lose = true;
                pause = true;
            }
            break;


        }
    }
}

GLvoid warriorBulletColision() {

    GLfloat bulletXMax = 0;
    GLfloat bulletXMin = 0;
    GLfloat bulletYMax = 0;
    GLfloat bulletYMin = 0;

    GLfloat warriorXMax = 0;
    GLfloat warriorXMin = 0;
    GLfloat warriorYMax = 0;
    GLfloat warriorYMin = 0;

    for (int k = 0; k < bullets.size(); k++) {


        bulletXMax = bullets[k]->getPosition()[0];
        bulletXMin = bullets[k]->getPosition()[0];
        bulletYMax = bullets[k]->getPosition()[1];
        bulletYMin = bullets[k]->getPosition()[1];


        for (int i = 0; i < warriors.size(); i++) {



            warriorXMax = warriors[i]->getPosition()[0] + 0.5f;
            warriorXMin = warriors[i]->getPosition()[0] - 0.5f;
            warriorYMax = warriors[i]->getPosition()[1] + 0.8f;
            warriorYMin = warriors[i]->getPosition()[1] - 0.8f;

            if (!(warriorXMax <= bulletXMin || warriorYMin >= bulletYMax || warriorXMin >= bulletXMax || warriorYMax <= bulletYMin)) {

                bullets.erase(bullets.begin() + k);
                warriors.erase(warriors.begin() + i);
                addScore();
                
            }
        }

        //cout << bulletYMax << endl;
    }
    
}

GLvoid shipBulletColision() {


    GLfloat shipXMax = ship->getPosition()[0] + 3.0f;
    GLfloat shipXMin = ship->getPosition()[0] - 3.0f;
    GLfloat shipYMax = ship->getPosition()[1] + 2.0f;
    GLfloat shipYMin = ship->getPosition()[1] - 2.0f;

    GLfloat bulletWarriorsXMax = 0;
    GLfloat bulletWarriorsXMin = 0;
    GLfloat bulletWarriorsYMax = 0;
    GLfloat bulletWarriorsYMin = 0;

    for (int k = 0; k < bulletsWarriors.size(); k++) {

        bulletWarriorsXMax = bulletsWarriors[k]->getPosition()[0] + 0.05f;
        bulletWarriorsXMin = bulletsWarriors[k]->getPosition()[0] - 0.05f;
        bulletWarriorsYMax = bulletsWarriors[k]->getPosition()[1] + 0.15f;
        bulletWarriorsYMin = bulletsWarriors[k]->getPosition()[1] - 0.15f;

        //cout << shipYMax << endl;

        if (!(bulletWarriorsXMin >= shipXMax || bulletWarriorsYMax <= shipYMin || bulletWarriorsXMax <= shipXMin || bulletWarriorsYMin >= shipYMax)) {

            bulletsWarriors.erase(bulletsWarriors.begin() + k);
            life--;
            k = 0;
            if (life < 1) {
                lose = true;
                pause = true;
            }
            break;

        }
    }
    
    
}

GLvoid deleteBullet() {
    //apaga as bullets que a nave dispara e falha 

    for (int i = 1; i < bullets.size(); i++) {
        if (bullets.at(i)->getPosition()[1] >= worldcoor[3])
            bullets.erase(bullets.begin() + i);
        
    }

    for (int k = 1; k < bulletsWarriors.size(); k++) {
        if (bulletsWarriors.at(k)->getPosition()[1] <= worldcoor[2])
            bulletsWarriors.erase(bulletsWarriors.begin() + k);

        //cout << bulletsWarriors.at(k)->getPosition()[1] << endl;
       
    }
    
}

GLvoid moveWarriors() {

    GLfloat xMin, xMax, yMin, yMax, * currentWarriorPos;

    xMin = worldcoor[1] + 0.5f;
    yMin = worldcoor[3] + 0.5f;
    xMax = worldcoor[0] - 0.5f;
    yMax = worldcoor[2] - 0.5f;   //dominio de colicao de todos os warriors 

    for (int i = 0; i < warriors.size(); i++) {

        currentWarriorPos = warriors[i]->getPosition();

        if (currentWarriorPos[0] < xMin)
            xMin = currentWarriorPos[0];

        if (currentWarriorPos[0] > xMax)
            xMax = currentWarriorPos[0];

        if (currentWarriorPos[1] < yMin)
            yMin = currentWarriorPos[1];

        if (currentWarriorPos[1] > yMax)
            yMax = currentWarriorPos[1];
    }


    // temos que adicionar ao x e ao y metade de inimigo em cada coor, 
    // porque a area de colisao inicial � feita com o centro de todos os warriors em volta

    xMin = xMin - 0.5f;
    xMax = xMax + 0.5f;
    yMin = yMin - 0.8f;
    yMax = yMax + 0.8f;




    for (int i = 0; i < warriors.size(); i++) {
        switch (dirWarrior) {
        
        case 0:

            if (xMin - 0.2f <= worldcoor[0]) {

            dirWarrior = 1;
            warriorsidecolision++;

            }
            
            if (warriorsidecolision == 5) {

                  dirWarrior = 2;

            }break;

        case 1:

            if (xMax + 0.2f >= worldcoor[1]) {
                dirWarrior = 0;

            }
            break;
        
        case 2:

            warriorsidecolision = 0;
            dirWarrior = 0;
           

        }break;

    }
    for (int i = 0; i < warriors.size(); i++) {
        warriors.at(i)->move(dirWarrior);

    }
}

GLvoid warriorBullet() {

    // random Warrior
    int randomWarrior = rand() % warriors.size();


    //get position do random Warrior
    float posRandomx = warriors.at(randomWarrior)->getPosition()[0];
    float posRandomy = warriors.at(randomWarrior)->getPosition()[1];

    bulletsWarriors.push_back(new Bullet(posRandomx, posRandomy));

    



}

// callback que vai tratar o codigo pessado: atualiza��es ...
GLvoid Idle(GLvoid) {

    

            if (gameTimerExpired) {

                gameTimerExpired = false;

                if(!pause){

                    if (warriors.size() < 1) {
                        creatWarriors();
                    }
                
              

                    //Caso n�o houvesse uma em casa vetor, o sistema rebentava ao iniciar
                    bulletsWarriors.push_back(new Bullet(40, -40));
                    bullets.push_back(new Bullet(40, -40));

                    moveWarriors();
                    warriorBulletColision();
                    shipBulletColision();
                    shipWarriorColision();
                    deleteBullet();

                    counter2++;
                    if (counter2 == 50) {
                        warriorBullet();
                        counter2 = 0;
                    }


                    //move ship bullet
                    for (int i = 1; i < bullets.size(); i++) {
                        bullets.at(i)->move(0);

                    }
                    //move warrior bullet
                    for (int k = 1; k < bulletsWarriors.size(); k++) 
                        bulletsWarriors.at(k)->move(1);


                    needsRedisplay = true;


                    if (needsRedisplay) {
                        glutPostRedisplay();
                        // tem que voltar ao estado inicial
                        needsRedisplay = false;
                    }

                }
            }
}


GLvoid draw(GLvoid) {

    //Callback desenho

// est� sempre a ser repetida assim que acaba o tempo
//gameTimerExpired vai executar a logica de jogo de x em x tempo
// s� damos uma vez a ordem que � necessario refrescar a tela


    glClearColor(0.0f, 0.1f, 0.2f, 0.0f);  // definir a cor de fundo 
    glClear(GL_COLOR_BUFFER_BIT);  // limpar o buffer de cor

    glMatrixMode(GL_PROJECTION); // sela��o da matriz de projecao
    glLoadIdentity(); // carregar a matriz identidade 

    gluOrtho2D(worldcoor[0], worldcoor[1], worldcoor[2], worldcoor[3]); // definicao da tela = mundo de desenho de -20 a 20

    glMatrixMode(GL_MODELVIEW); // Matriz de visualiza��o
    glLoadIdentity();



    ship->draw(); // desenha a ship


    // desenha os warriors
    for (GLint i = 0; i < warriors.size(); i++)
        warriors[i]->draw();

    // desenha as bullets da ship
    for (GLint j = 1; j < bullets.size(); j++)
        bullets[j]->draw();

    // desenha as bullets do warrior
    for (GLint k = 1; k < bulletsWarriors.size(); k++)
        bulletsWarriors[k]->draw();

    drawScore();
    drawLife();

    if (lose)
        drawLose();

    glutSwapBuffers(); // So fa�o a troca de buffers quando acabo de desenhar tudo. 

}




int main(int argc, char** argv)
{
    glutInit(&argc, argv); // Inicializa��o do freeGLUT

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE); // Inicializa��o do modo de renderiza��o #### GLUT_DOUBLE -> Modo de duplo buffer, tenho que trocar os buffers para aparecer o desenho

    glutInitWindowSize(1000, 1000); // Definir o tamanho da janela

    glutInitWindowSize(0, 0); // Posi��o inicial da janela

    glutCreateWindow("In vis�o all ninja "); // Criar a janela



    // Registo das Callback

    // callback desenho


    glutDisplayFunc(draw);



    // callback teclado

    glutKeyboardFunc(keyboard);

    /*callback register
    * 
    1000 x 60 hz = 17 
    Temporizador s� � executado uma vez, e � renovado novamente na fun��o gameTimer
    numero de mls a funcao e um parametro */

    glutTimerFunc(17, gameTimer, 0);


    //callbak idle

    glutIdleFunc(Idle);

    creatWarriors();

    glutMainLoop(); // Iniciar o ciclo de eventos, so reage aos eventos programados


    return 0;

}



