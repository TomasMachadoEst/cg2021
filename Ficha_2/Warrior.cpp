#include "Warrior.h"


extern GLfloat worldcoor[4];



Warrior::Warrior() {

	position[0] = position[1] = 0.0f;
	
	GLfloat speedx = 0.20f;
	GLfloat speedy = 2.0f;

}

Warrior::Warrior(GLfloat px, GLfloat py) {

	position[0] = px;
	position[1] = py;
	speedx = 0.20f;
	speedy = 2.0f;



}


//Devolve um array com duas posicoes atraves de um apontador

GLfloat* Warrior::getPosition(GLvoid) {

	return position;
}





GLboolean Warrior::move(GLint dir) {

	switch (dir)
	{
	case 0: // left
		if ((position[0] - speedx - 0.5f) > worldcoor[0])
		{
			position[0] -= speedx;
			return true;
			
		}break;

	case 1: // right
		if ((position[0] + speedx + 0.5f) < worldcoor[1])
		{
			position[0] += speedx;
			return true;
			
		}break;

	case 2: // down
		if ((position[1] - speedy - 0.8f) > worldcoor[2])
		{
			position[1] -= speedy;
			return true;

		}break;

	case 3: // up
		if ((position[1] + speedy + 0.8f) < worldcoor[3])
		{
			position[1] += speedy;
			return true;

		}break;

	default:
		return false;
		break;

	}return false;
}


GLvoid Warrior :: warriorBody() {

	glColor3f(0.5f, 0.0f, 1.0f);

	glBegin(GL_QUADS); {
		      
		glVertex2f(0.2f, 0.0f);
		glVertex2f(0.0f, 1.0f);
		glVertex2f(1.0f, 1.0f);
		glVertex2f(0.8f, 0.0f);

	}glEnd();


}


GLvoid Warrior::warriorGun() {

	glColor3f(1.0f, 0.6f, 0.0f);

	glBegin(GL_QUADS); {

		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 0.3f);
		glVertex2f(0.2f, 0.3f);
		glVertex2f(0.2f, 0.0f);

	}glEnd();


}


GLvoid Warrior::draw() {
	// em vez de ter glMatrixIden temos translacoes controladas com pilha




	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glTranslatef(position[0], position[1], 0.0f);
	glPushMatrix();
	
	
	glTranslatef(-0.5f, -0.5f, 0.0f);
	warriorBody();
	glPopMatrix();

	glTranslatef(-0.1f, -0.8f, 0.0f);
	warriorGun();
	glPopMatrix();
	glPopMatrix();
	

}

Warrior::~Warrior() {

}