#pragma once

#include "Sistema.h"

class Warrior
{
private:
	GLfloat position[2]; //posicao inicial 
	GLfloat speedx,speedy;

	GLvoid  warriorBody();
	GLvoid  warriorGun();

public:

	Warrior();
	GLboolean move(GLint);
	Warrior(GLfloat, GLfloat);
	GLfloat * getPosition(GLvoid);
	GLvoid draw(GLvoid);
	~Warrior();
};

